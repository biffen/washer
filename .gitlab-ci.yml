---
default:
  cache:
    paths:
      - .cache
  image: golang

variables:
  GOCACHE: ${CI_PROJECT_DIR}/.cache/go
  GOMODCACHE: ${CI_PROJECT_DIR}/.cache/gomod

docs:
  stage: build
  needs: []
  image: sphinxdoc/sphinx
  before_script:
    - pip install -r docs/requirements.txt
  script:
    - >-
      sphinx-build
      --color
      --keep-going
      -D release="${CI_COMMIT_TAG:-DEV}"
      -D version="${CI_COMMIT_TAG:-DEV}"
      -W
      -b html
      -d .cache/doctrees
      -j auto
      -n
      ${CI_DEBUG_TRACE:+-vvv}
      docs/
      html/
  cache: {}
  artifacts:
    expose_as: Docs
    paths:
      - html/

go build:
  stage: build
  parallel:
    matrix:
      # - GOOS: aix
      #   GOARCH:
      #     - ppc64
      # - GOOS: android
      #   GOARCH:
      #     - 386
      #     - amd64
      #     - arm
      #     - arm64
      - GOOS: darwin
        GOARCH:
          - amd64
          # - arm64
      # - GOOS: dragonfly
      #   GOARCH:
      #     - amd64
      # - GOOS: freebsd
      #   GOARCH:
      #     - 386
      #     - amd64
      #     - arm
      #     - arm64
      # - GOOS: illumos
      #   GOARCH:
      #     - amd64
      - GOOS: linux
        GOARCH:
          - 386
          - amd64
          # - arm
          # - arm64
          # - mips
          # - mips64
          # - mips64le
          # - mipsle
          # - ppc64
          # - ppc64le
          # - riscv64
          # - s390x
      # - GOOS: netbsd
      #   GOARCH:
      #     - 386
      #     - amd64
      #     - arm
      #     - arm64
      # - GOOS: openbsd
      #   GOARCH:
      #     - 386
      #     - amd64
      #     - arm
      #     - arm64
      # - GOOS: plan9
      #   GOARCH:
      #     - 386
      #     - amd64
      #     - arm
      # - GOOS: solaris
      #   GOARCH:
      #     - amd64
      - GOOS: windows
        GOARCH:
          # - 386
          - amd64
          # - arm
  needs: []
  interruptible: true
  script:
    - go build -o "washer-${GOOS}-${GOARCH}" ./
  artifacts:
    paths:
      - 'washer-*-*'

go test:
  stage: test
  needs: []
  interruptible: true
  before_script:
    - set +o pipefail
    - go install github.com/jstemmer/go-junit-report@latest
    - go install github.com/t-yuki/gocover-cobertura@latest
    - go install golang.org/x/tools/cmd/cover@latest
  script:
    - >-
      go test
      -covermode atomic
      -coverpkg "$(go list)/..."
      -coverprofile coverage.txt
      -race
      -v
      ./...
      | tee test.out
    - go-junit-report -set-exit-code < test.out > gotest.junit.xml
  after_script:
    - go tool cover -func coverage.txt
    - go tool cover -html coverage.txt -o coverage.html
    - gocover-cobertura < coverage.txt > coverage.xml
  coverage: >-
    /^total:\s*\(statements\)\s*\d+.\d+%$/
  artifacts:
    expose_as: Go Coverage
    paths:
      - coverage.html
    reports:
      coverage_report:
        coverage_format: cobertura
        path: coverage.xml
      junit:
        - '*junit.xml'
    when: always

lint:
  stage: test
  needs:
    - washer docker build
  image: ${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHA}
  variables:
    WASHER_DEBUG: ${CI_DEBUG_TRACE}
  interruptible: true
  script:
    - >-
      washer check
      --output -
      --output washer.codeclimate.json=codeclimate
  cache: {}
  artifacts:
    paths:
      - '*codeclimate.json'
    reports:
      codequality:
        - '*codeclimate.json'
    when: always

pages:
  stage: deploy
  rules: &release
    - if: $CI_COMMIT_TAG =~ /^v\d+\.\d+\.\d+/
  dependencies:
    - docs
  script:
    - mv html public
  cache: {}
  artifacts:
    paths:
      - public

release:
  stage: deploy
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  rules: *release
  script:
    - ''
  release:
    name: 'Release $CI_COMMIT_TAG'
    description: $CI_COMMIT_TAG
    tag_name: $CI_COMMIT_TAG
    ref: $CI_COMMIT_TAG
  cache: {}

washer docker build:
  stage: .pre
  needs: []
  image: docker:20
  services:
    - docker:20-dind
  variables:
    DOCKER_BUILDKIT: 1
    DOCKER_HOST: tcp://docker:2376
    DOCKER_TLS_CERTDIR: '/certs'
  before_script:
    - set -u
    - |-
      printf '%s' "${CI_REGISTRY_PASSWORD}" |
        docker login \
          --password-stdin \
          --username "${CI_REGISTRY_USER}" \
          "${CI_REGISTRY}"
  script:
    - >-
      docker build
      --build-arg  BUILDKIT_INLINE_CACHE=1
      --cache-from "${CI_REGISTRY_IMAGE}:${CI_COMMIT_BEFORE_SHA}"
      --cache-from "${CI_REGISTRY_IMAGE}:${CI_COMMIT_REF_SLUG}"
      --cache-from "${CI_REGISTRY_IMAGE}:latest"
      --pull
      --tag        "${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHA}"
      .
    - docker push "${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHA}"
    - >-
      docker tag
      "${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHA}"
      "${CI_REGISTRY_IMAGE}:${CI_COMMIT_REF_SLUG}"
    - docker push "${CI_REGISTRY_IMAGE}:${CI_COMMIT_REF_SLUG}"
  cache: {}
