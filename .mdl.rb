all

rule 'MD003', :style => :consistent

rule 'MD004', :style => :consistent

rule 'MD007', :indent => 4

rule 'MD013',
     :code_blocks => false,
     :tables => false

exclude_rule 'MD024'

rule 'MD026', :punctuation => '.,;:'

rule 'MD029', :style => 'ordered'

rule 'MD030',
     :ul_single => 3,
     :ol_single => 2,
     :ul_multi => 3,
     :ol_multi => 2

rule 'MD035', :style => '---'
