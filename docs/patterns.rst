.. _patterns:

********
Patterns
********

Patterns are used when matching files to be included or excluded. The type of
pattern is specified as a YAML tag. The default type is glob.

Glob
====

Glob patterns are like regular Unix `globs`_, but with support for ``**`` to
match across directories.

Patterns that begin with a ``/`` match from the Washer root directory. Others
match anywhere.

Washer uses `bmatcuk/doublestar`_ for the glob implementation.

.. _bmatcuk/doublestar: https://github.com/bmatcuk/doublestar#patterns
.. _globs: https://en.wikipedia.org/wiki/Glob_(programming)

Examples
--------

``*.md``
   Any file ending in ``.md``.

``/README*``
   Files in the root directory that begin with ``README``.

.. NOTE::

   Glob patterns *can* be tagged with ``!glob`` (e.g. ``!glob *.txt``), but
   don’t have to.

Regex
=====

Regular expression patterns have the tag ``!re``.

The rest is a regular expression matched against the path (from the root
directory) of a file.

Examples
--------

``!re '\.ya?ml$'``
  Any file ending in ``.yaml`` or ``.yml``.

Content
=======

Content patterns have the tag ``!content``.

The rest is a regular expression matched against the whole content of a file.

Examples
--------

``!content '^%YAML\s+\d+(?:\.\d+)?'``
   A file that begins with ``%YAML`` followed by a version number.

Shebang
=======

Shebang patterns have the tag ``!shebang``.

The rest is a regular expression matched against the `shebang`_ line of a file,
if any.

.. _shebang: https://en.wikipedia.org/wiki/Shebang_(Unix)

Examples
--------

``!shebang '.*'``
   *Any* file with a shebang.

   Won’t match a file *without* a shebang.

``!shebang '/(?:ba)?sh(\s|$)'``
   A shebang for ``bash`` or ``sh``.
