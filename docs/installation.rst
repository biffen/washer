************
Installation
************

`Go`_ is needed. Run the following to install Washer:

.. code-block:: sh

   go install gitlab.com/biffen/washer@latest

.. _Go: https://go.dev
