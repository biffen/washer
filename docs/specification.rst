*************
Specification
*************

.. highlight:: yaml

At the core of Washer’s functionality is the specification. It should be in the
root directory of your project and be called ``washer.yaml`` (it may be prefixed
with a ``.`` if you prefer to hide it) (``.yml`` is also accepted).

Here is an example specification:

.. literalinclude:: example-specification.yaml
   :caption: ``washer.yaml``

``exclude``
===========

Washer will apply tools to all files, recursively, within the root directory. To
exclude files and directories list :ref:`patterns` here.
syntax.

``extends``
===========

A specification can extend other specifications. ``extends`` lists
:ref:`Locations` of specifications that will be merged with this specification.

Merging is deep, so values in an extended specification can be overridden by the
specification that extends it.

Extended specifications can themselves extend yet other specifications. There is
no limit to the number of ‘levels’, other than computing power.

.. DANGER::

   Extending a specification means executing code from somewhere else. Don’t
   extend specifications that you don’t trust.

``tools``
=========

A map of tools. Each tool has the following configuration:

``check`` and ``fix``
---------------------

Both are commands to run.

``check`` is a command that only tests. It shouldn’t modify any files.

``fix`` is a command that can change files to fix them.

Both ``check`` and ``fix`` are optional (but a tool with neither is useless).

Commands are YAML maps with the following keys:

``command`` (list of strings)
   The command to run.

   .. NOTE::

      Commands do *not* get passed to a shell, so no shell constructs can be
      used here, e.g. environment variables, glob patterns, loops, etc.

``exit`` (enum)
   *Not set*
      Normally a zero exit code means the command succeeded, and a none-zero
      exit code means it failed.

   ``ignore``
      Never treat *any* exit code as a failure.

   ``inverse``
      Treat zero as failure and non-zero as success.

``file`` (enum)
   *Not set*
      Normally, unless the command is global (see below), the path of the file
      to be checked or fixed is appended to the command.

   ``append``
      The default. Append the path as its own arguments.

   ``discard``
      Don’t append the path.

``global`` (boolean)
   If true then the command is not run once per matching file. Instead, when
   there’s *at least one* matching file then the command is run *once*. No file
   path gets appended.

``output`` (enum)
   *Not set*
      Output doesn’t affect the success of the command.

   ``empty``
      Any output at all (except trailing linefeeds) is treated as a failure.

Short Syntax
^^^^^^^^^^^^

Commands that only have a ``command`` list can be written as just a YAML list,
e.g:

.. code-block:: yaml

   tool:
     check:
       - some-tool
       - --some-option

Is the same as:

.. code-block:: yaml

   tool:
     check:
       command:
         - some-tool
         - --some-option

``include`` and ``exclude``
---------------------------

Both are lists of :ref:`patterns`.

Most tools should only be run on specific files. A tool will be run once for
each file that matches *any* pattern in ``include`` but *doesn’t* match *any*
pattern in ``exclude``.

If a tool has no ``include`` then *all* files are included. (``exclude`` still
applies.)

``parse``
---------

A string.

Tells Washer how to (attempt to) parse the tool’s output.

``gnu`` (default)
  GNU-style errors, e.g. ``file:line:column: message``. See `Errors (GNU Coding
  Standards) <https://www.gnu.org/prep/standards/html_node/Errors.html>`_ for
  more details.

``junit``
  JUnit XML.
