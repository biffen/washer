# syntax=docker/dockerfile:1.3

FROM alpine:edge AS edge

FROM alpine:3.18

ENV                               \
  PATH="${PATH}:/root/go/bin/"    \
  PIP_COMPILE=1                   \
  PIP_DISABLE_PIP_VERSION_CHECK=1 \
  PIP_NO_CACHE_DIR=1

COPY --from=edge /etc/apk/repositories /etc/apk/repositories.edge
RUN sed 's/^/@edge /g' < /etc/apk/repositories.edge >> /etc/apk/repositories \
  &&                                                                         \
  rm /etc/apk/repositories.edge

RUN apk --no-cache add \
  curl'=~8'            \
  git'=~2'             \
  go@edge'=~1'         \
  npm'=~9'             \
  py3-pip'=~23'        \
  python3-dev'=~3'     \
  ruby'=~3'            \
  shellcheck'=~0.9'    \
  yamllint'=~1'        \
  &&                   \
  shellcheck --version \
  &&                   \
  yamllint --version

WORKDIR /work/

RUN curl                                                                                            \
  --fail                                                                                            \
  --location                                                                                        \
  --output /usr/local/bin/hadolint                                                                  \
  --show-error                                                                                      \
  --silent                                                                                          \
  "https://github.com/hadolint/hadolint/releases/download/v2.12.0/hadolint-$(uname -s)-$(uname -m)" \
  &&                                                                                                \
  chmod +x /usr/local/bin/hadolint                                                                  \
  &&                                                                                                \
  hadolint --version

RUN gem install --no-document --no-user-install \
  mdl':0.12'                                    \
  &&                                            \
  mdl --version

RUN set -ex                                                              ; \
  go install github.com/golangci/golangci-lint/cmd/golangci-lint@v1.54.2 ; \
  golangci-lint version                                                  ; \
  go install golang.org/x/tools/cmd/goimports@latest                     ; \
  command -v goimports                                                   ; \
  go install gitlab.com/biffen/gosrt@latest                              ; \
  gosrt -h                                                               ; \
  go install mvdan.cc/gofumpt@latest                                     ; \
  gofumpt -version

RUN npm install                                     \
  --global                                          \
  --no-audit                                        \
  --no-fund                                         \
  --no-save                                         \
  lodash'@4'                                        \
  prettier'@3'                                      \
  textlint'@13'                                     \
  textlint-filter-rule-comments'@1'                 \
  textlint-plugin-rst'@0'                           \
  textlint-rule-abbr-within-parentheses'@1'         \
  textlint-rule-alex'@3'                            \
  textlint-rule-apostrophe'@2'                      \
  textlint-rule-common-misspellings'@1'             \
  textlint-rule-date-weekday-mismatch'@1'           \
  textlint-rule-diacritics'@1'                      \
  textlint-rule-en-max-word-count'@1'               \
  textlint-rule-footnote-order'@1'                  \
  textlint-rule-max-doc-width'@1'                   \
  textlint-rule-ng-word'@1'                         \
  textlint-rule-no-dead-link'@4'                    \
  textlint-rule-no-empty-section'@1'                \
  textlint-rule-no-nfd'@1'                          \
  textlint-rule-no-start-duplicated-conjunction'@2' \
  textlint-rule-period-in-list-item'@0'             \
  textlint-rule-rousseau'@1'                        \
  textlint-rule-stop-words'@2'                      \
  textlint-rule-terminology'@2'                     \
  &&                                                \
  prettier --version                                \
  &&                                                \
  textlint --version

RUN pip3 install               \
  black'==23.9'                \
  docutils-ast-writer'==0.1.1' \
  mypy'==1.5'                  \
  pylint'==2.17'               \
  &&                           \
  black --version              \
  &&                           \
  mypy --version               \
  &&                           \
  pylint --version

RUN --mount=target=. go install . && washer --help >/dev/null
