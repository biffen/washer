package main_test

import (
	"context"
	"os"
	"regexp"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	errör "gitlab.com/biffen/error"
	washer "gitlab.com/biffen/washer"
)

var cases = map[string]Case{
	"Check Bad File": {
		Command: []string{"check", "bad.txt"},
		Exit:    3,
	},

	"Check Good File": {
		Command: []string{"check", "good.txt"},
	},

	"Check Good Tool": {
		Command: []string{"check", "-t", "always-good"},
	},

	"Check Good Tool, Bad File": {
		Command: []string{"check", "-t", "always-good", "bad.txt"},
	},

	"Check Good Tool, Good File": {
		Command: []string{"check", "-t", "always-good", "good.txt"},
	},

	"Check One Tool, Bad File": {
		Command: []string{"check", "-t", "mandatory", "bad.txt"},
		Exit:    3,
	},

	"Check One Tool, Good File": {
		Command: []string{"check", "-t", "mandatory", "good.txt"},
	},

	"Check": {
		Command: []string{"check"},
		Exit:    3,
	},

	"Dump Spec Path": {
		Command: []string{"dump", "spec", "-f"},
		Out:     regexp.MustCompile(`^.*\bwasher\.yaml\n`),
	},

	"Dump Spec": {
		Command: []string{"dump", "spec"},
		Out:     regexp.MustCompile(`(?s)\bexclude:.*/bad/`),
	},

	"Fix Dry-Run": {
		Command: []string{"fix", "-n"},
		Err:     regexp.MustCompile(`(?m)^washer: \+ true washer\.yaml$`),
	},

	"Help": {
		Command: []string{"-h"},
		Out:     regexp.MustCompile(`Usage`),
	},

	"Just Run": {
		Exit: 3,
	},

	"Unknown File": {
		Command: []string{"check", "non-existent-file"},
		Exit:    2,
	},

	"Unknown Option": {
		Command: []string{"--unknown-option"},
		Err:     regexp.MustCompile(`\bunknown option\b.*--unknown-option\b`),
		Exit:    2,
	},

	"Unknown Tool": {
		Command: []string{"check", "-t", "non-existent-tool"},
		Exit:    2,
	},
}

//nolint:paralleltest // Shouldn’t be run in parallel.
func Test(t *testing.T) {
	t.Setenv("PS4", "+ ")

	ctx := context.Background()

	//nolint:paralleltest // Shouldn’t be run in parallel.
	for name, test := range cases {
		name, test := name, test

		t.Run(name, func(t *testing.T) {
			if test.After != nil {
				defer test.After(t, &test)
			}

			if test.Before != nil {
				test.Before(t, &test)
			}

			var exit int
			stdout, stderr, err := testEnv(
				"test",
				nil,
				append([]string{"washer"}, test.Command...),
				func() {
					exit = washer.Main(ctx)
				})

			require.NoError(t, err)

			ok := assert.Equal(t, test.Exit, exit, "exit code")

			for _, s := range [...]struct {
				Name    string
				Pattern *regexp.Regexp
				Bytes   []byte
			}{
				{"STDOUT", test.Out, stdout},
				{"STDERR", test.Err, stderr},
			} {
				if s.Pattern != nil {
					ok = ok && assert.Regexp(
						t,
						s.Pattern,
						string(s.Bytes),
						"%s (%s)",
						s.Name,
						test.Command,
					)
				}

				if !ok {
					t.Logf("%s: %q", s.Name, s.Bytes)
				}
			}
		})
	}
}

func testEnv(
	wd string,
	stdin []byte,
	args []string,
	f func(),
) (stdout, stderr []byte, err error) {
	if wd != "" {
		var oldDir string

		if oldDir, err = os.Getwd(); err != nil {
			return
		}

		if err = os.Chdir(wd); err != nil {
			return
		}

		defer func() {
			err = errör.Composite(err, os.Chdir(oldDir))
		}()
	}

	oldIn, oldOut, oldErr, oldArgs := os.Stdin, os.Stdout, os.Stderr, os.Args
	defer func() {
		os.Stdin, os.Stdout, os.Stderr, os.Args = oldIn, oldOut, oldErr, oldArgs
	}()

	var tmp [3]*os.File

	for i := range [3]struct{}{} {
		var f *os.File

		if f, err = os.CreateTemp("", ""); err != nil {
			return
		}

		tmp[i] = f

		defer func() {
			err = errör.Composite(err, os.Remove(f.Name()))
		}()
	}

	if err = os.WriteFile(tmp[0].Name(), stdin, 0o600); err != nil {
		return
	}

	os.Stdin = tmp[0]
	os.Stdout = tmp[1]
	os.Stderr = tmp[2]

	defer func() {
		var stdoutErr, stderrErr error
		stdout, stdoutErr = os.ReadFile(tmp[1].Name())
		stderr, stderrErr = os.ReadFile(tmp[2].Name())
		err = errör.Composite(err, stdoutErr, stderrErr)
	}()

	os.Args = args

	f()

	return
}

type Case struct {
	Before, After func(*testing.T, *Case)
	Command       []string
	Out, Err      *regexp.Regexp
	Exit          int
}
