package file_test

import (
	"fmt"
	"os"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/biffen/washer/internal/file"
)

const this = "file_test.go"

func TestFile_Abs(t *testing.T) {
	t.Parallel()

	wd, err := os.Getwd()
	require.NoError(t, err)

	f, err := file.New("", this)
	require.NoError(t, err)
	assert.Equal(t, filepath.Join(wd, this), f.Abs())
}

func TestFile_Compare(t *testing.T) {
	t.Parallel()

	f1, err := file.New("", "1")
	require.NoError(t, err)
	f1b, err := file.New("", "1")
	require.NoError(t, err)
	f2, err := file.New("", "2")
	require.NoError(t, err)

	assert.False(t, f1.Equal(f2))
	//nolint:gocritic // On purpose
	assert.Equal(t, f1.Compare(f1), 0)
	assert.Equal(t, f1.Compare(f1b), 0)
	assert.False(t, f2.Equal(f1))
	assert.Greater(t, f2.Compare(f1), 0)
	//nolint:gocritic // On purpose
	assert.True(t, f1.Equal(f1))
	assert.True(t, f1.Equal(f1b))
	assert.Less(t, f1.Compare(f2), 0)
	assert.True(t, f1b.Equal(f1))
	//nolint:gocritic // On purpose
	assert.True(t, f2.Equal(f2))
}

func TestFile_FirstLine(t *testing.T) {
	t.Parallel()

	f, err := file.New("", this)
	require.NoError(t, err)

	firstLine, err := f.FirstLine()
	assert.NoError(t, err)
	assert.Equal(t, "package file_test", firstLine)
}

func TestFile_IsDir(t *testing.T) {
	t.Parallel()

	f, err := file.New("", "")
	require.NoError(t, err)

	assert.True(t, f.IsDir())
}

func TestFile_Name(t *testing.T) {
	t.Parallel()

	f, err := file.New("", "")
	require.NoError(t, err)

	assert.Equal(t, "file", f.Name())
}

func TestFile_Rel(t *testing.T) {
	t.Parallel()

	if os.PathSeparator != '/' {
		t.Skip("Weird OS")
	}

	f, err := file.New("", this)
	require.NoError(t, err)

	for _, pair := range [...][2]string{
		{this, ""},
		{"file/" + this, ".."},
		{"internal/file/" + this, "../.."},
		{"../file/" + this, "../pattern"},
	} {
		rel, err := f.Rel(pair[1])
		assert.NoError(t, err)
		assert.Equal(t, pair[0], rel)
	}
}

func TestNew(t *testing.T) {
	t.Parallel()

	wd, err := os.Getwd()
	require.NoError(t, err)

	for _, c := range [...]struct {
		WD, Path, ExpectedWD, ExpectedPath string
	}{
		{"", this, wd, this},
		{"", "dir/", wd, "dir/"},
		{"", ".", wd, ""},
		{"", "./", wd, ""},
		{"/foo/bar", "baz", "/foo/bar", "baz"},
		{"/foo/bar", "/foo/bar/baz", "/foo/bar", "baz"},
	} {
		c := c

		t.Run(fmt.Sprintf("%q, %q", c.WD, c.Path), func(t *testing.T) {
			t.Parallel()

			f, err := file.New(c.WD, c.Path)
			assert.NoError(t, err)
			assert.Equal(t, c.ExpectedWD, f.WD, "working directory")
			assert.Equal(t, c.ExpectedPath, f.Path, "path")
		})
	}
}
