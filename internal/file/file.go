package file

import (
	"bufio"
	"errors"
	"flag"
	"fmt"
	"io"
	"io/fs"
	"os"
	"path/filepath"
	"strings"
	"sync"

	errör "gitlab.com/biffen/error"
	"gitlab.com/biffen/washer/internal/util"
)

var (
	_ flag.Value          = (*File)(nil)
	_ fmt.Stringer        = (*File)(nil)
	_ util.Element[*File] = (*File)(nil)
)

type File struct {
	WD, Path string

	firstLine *string
	info      *os.FileInfo
	mtx       sync.Mutex
}

func New(wd, path string) (f *File, err error) {
	if wd == "" {
		wd = "."
	}

	if wd, err = filepath.Abs(wd); err != nil {
		return
	}

	f = &File{
		WD:   wd,
		Path: filepath.Clean(path),
	}

	if filepath.IsAbs(f.Path) {
		f.Path, err = filepath.Rel(f.WD, f.Path)
		if err != nil {
			return
		}
	}

	if f.Path != "" {
		if !strings.HasSuffix(f.Path, string(os.PathSeparator)) &&
			(strings.HasSuffix(path, string(os.PathSeparator)) ||
				f.IsDir()) {
			f.Path += string(os.PathSeparator)
		}
	}

	if f.Path == "./" {
		f.Path = ""
	}

	return
}

func (f *File) Abs() string {
	return filepath.Join(f.WD, f.Path)
}

func (f *File) Compare(other *File) int {
	return strings.Compare(f.Abs(), other.Abs())
}

func (f *File) Equal(other *File) bool {
	return f.Abs() == other.Abs()
}

func (f *File) Exists() (bool, error) {
	_, err := f.Info()

	switch {
	case err == nil:
		return true, nil

	case errors.Is(err, fs.ErrNotExist):
		return false, nil

	default:
		return false, err
	}
}

func (f *File) FirstLine() (line string, err error) {
	f.mtx.Lock()
	defer f.mtx.Unlock()

	if f.firstLine == nil {
		var (
			file *os.File
			path = f.Abs()
		)

		file, err = os.Open(path)
		if err != nil {
			err = fmt.Errorf(
				"failed to read first line of file %q: %w",
				path,
				err,
			)

			return
		}

		defer func() {
			err = errör.Composite(err, file.Close())
		}()

		var (
			reader = bufio.NewReader(file)
			str    string
		)

		if str, err = reader.ReadString('\n'); err != nil {
			if errors.Is(err, io.EOF) {
				err = nil
			} else {
				return
			}
		}

		str = strings.TrimRight(str, "\n")

		f.firstLine = &str
	}

	line = *f.firstLine

	return
}

func (f *File) Info() (info os.FileInfo, err error) {
	f.mtx.Lock()
	defer f.mtx.Unlock()

	if f.info == nil {
		info, err = os.Stat(f.Abs())
		if err != nil {
			return
		}

		f.info = &info
	}

	info = *f.info

	return
}

func (f *File) IsDir() bool {
	if info, err := f.Info(); err == nil {
		return info.IsDir()
	}

	return false
}

func (f *File) Name() string {
	return filepath.Base(f.Abs())
}

func (f *File) Rel(to string) (rel string, err error) {
	to, err = filepath.Abs(to)
	if err != nil {
		return
	}

	rel, err = filepath.Rel(to, f.Abs())

	return
}

func (f *File) Set(path string) error {
	wd, err := os.Getwd()
	if err != nil {
		return fmt.Errorf("failed to get working directory: %w", err)
	}

	*f = File{
		WD:   wd,
		Path: path,
	}

	return nil
}

func (f *File) String() string {
	rel, err := f.Rel("")
	if err != nil {
		return fmt.Sprintf("<%v>", err)
	}

	return rel
}
