package backup_test

import (
	"context"
	"os"
	"path"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/biffen/washer/internal/backup"
)

//nolint:paralleltest,tparallel // Nope.
func TestBackup(t *testing.T) {
	t.Parallel()

	ctx := context.Background()

	tmp, err := os.MkdirTemp("", "example")
	require.NoError(t, err)

	defer func() {
		assert.NoError(t, os.RemoveAll(tmp))
	}()

	orig := path.Join(tmp, "orig")

	//nolint:paralleltest,tparallel // Nope.
	t.Run("no changes", func(t *testing.T) {
		write(t, orig, "original")

		b, err := backup.New(ctx, orig, "test")
		require.NoError(t, err)
		require.NotNil(t, b)

		hasContent(t, b.String(), "original")

		assert.NoError(t, b.Close())

		hasContent(t, orig, "original")
		doesNotExist(t, b.String())
	})

	//nolint:paralleltest,tparallel // Nope.
	t.Run("changes", func(t *testing.T) {
		write(t, orig, "original")

		b, err := backup.New(ctx, orig, "test")
		require.NoError(t, err)
		require.NotNil(t, b)

		hasContent(t, b.String(), "original")

		write(t, orig, "new")

		hasContent(t, orig, "new")
		hasContent(t, b.String(), "original")

		assert.NoError(t, b.Close())

		hasContent(t, orig, "new")
		hasContent(t, b.String(), "original")
	})

	//nolint:paralleltest,tparallel // Nope.
	t.Run("restore", func(t *testing.T) {
		write(t, orig, "original")

		b, err := backup.New(ctx, orig, "test")
		require.NoError(t, err)
		require.NotNil(t, b)

		hasContent(t, b.String(), "original")

		write(t, orig, "new")

		hasContent(t, orig, "new")
		hasContent(t, b.String(), "original")

		assert.NoError(t, b.Restore())

		hasContent(t, orig, "original")
		doesNotExist(t, b.String())

		assert.NoError(t, b.Close())

		hasContent(t, orig, "original")
		doesNotExist(t, b.String())
	})
}

func doesNotExist(t *testing.T, p string) {
	t.Helper()

	_, err := os.Stat(p)

	assert.True(t, os.IsNotExist(err), "file does not exist")
}

func hasContent(t *testing.T, p, expected string) {
	t.Helper()

	content, err := os.ReadFile(p)
	if assert.NoError(t, err, "can read file") {
		assert.Equal(t, expected, string(content), "expected file content")
	}
}

func write(t *testing.T, p, content string) {
	t.Helper()

	err := os.WriteFile(p, []byte(content), 0o600)
	require.NoError(t, err)
}
