package pattern

import (
	"context"
	"fmt"
	"os"
	"regexp"

	"gitlab.com/biffen/washer/internal/file"
)

var _ Matcher = (*Content)(nil)

const TypeContent = "content"

type Content struct {
	regex *regexp.Regexp
}

func NewContent(str string) (*Content, error) {
	regex, err := compileRegexp(str)
	if err != nil {
		return nil, err
	}

	return &Content{
		regex: regex,
	}, nil
}

func (c *Content) Match(
	_ context.Context,
	f *file.File,
) (match bool, err error) {
	var content []byte

	content, err = os.ReadFile(f.Abs())
	if err != nil {
		err = fmt.Errorf(
			"failed to read file when matching content: %w",
			err,
		)

		return
	}

	match = c.regex.Match(content)

	return
}
