package pattern

import (
	"context"
	"regexp"

	"gitlab.com/biffen/washer/internal/file"
)

var _ Matcher = (*Regex)(nil)

const TypeRegex = "re"

type Regex struct {
	regex *regexp.Regexp
	str   string
}

func NewRegex(str string) (*Regex, error) {
	regex, err := compileRegexp(str)
	if err != nil {
		return nil, err
	}

	return &Regex{
		str:   str,
		regex: regex,
	}, nil
}

func (r *Regex) Match(_ context.Context, f *file.File) (bool, error) {
	return r.regex.MatchString(f.Path), nil
}
