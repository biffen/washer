package pattern

import (
	"context"
	"reflect"

	"gitlab.com/biffen/washer/internal/errors"
	"gitlab.com/biffen/washer/internal/file"
	"gitlab.com/biffen/washer/internal/util"
	"gopkg.in/yaml.v3"
)

var (
	_ util.Merger      = (*Set)(nil)
	_ yaml.Marshaler   = (*Set)(nil)
	_ yaml.Unmarshaler = (*Set)(nil)
)

type Set struct {
	util.SortedSet[*Pattern]
}

func (s *Set) MarshalYAML() (interface{}, error) {
	return s.Slice(), nil
}

func (s *Set) Match(
	ctx context.Context,
	f *file.File,
) (ok bool, firstMatch *Pattern, err error) {
	err = s.Each(ctx, func(pattern *Pattern) bool {
		ok, err = pattern.Match(ctx, f)
		if ok {
			firstMatch = pattern

			return false
		}

		return err == nil
	})

	return
}

func (s *Set) Merge(src reflect.Value) (err error) {
	other, ok := src.Interface().(*Set)
	if !ok {
		panic(errors.Internal(nil))
	}

	if other == nil {
		return
	}

	s.Insert(context.Background(), other.Slice()...)

	return
}

func (s *Set) UnmarshalYAML(value *yaml.Node) (err error) {
	var tmp []*Pattern

	if err = value.Decode(&tmp); err != nil {
		return
	}

	s.Insert(context.Background(), tmp...)

	return
}
