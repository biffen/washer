package pattern_test

import (
	"context"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/biffen/washer/internal/file"
	"gitlab.com/biffen/washer/internal/pattern"
)

func TestContent(t *testing.T) {
	t.Parallel()

	const (
		yaml      = `(?m)^%YAML\s+\d+\.\d+`
		generated = `(?m)^// Code generated .* [D]O NOT EDIT\.$`
	)

	ctx := context.Background()

	for _, tt := range [...]struct {
		Pattern, Path string
		Expected      bool
	}{
		{generated, "../../washer.yaml", false},
		{yaml, "../../COPYING", false},
		{yaml, "../../washer.yaml", true},
	} {
		tt := tt

		t.Run(fmt.Sprintf("%s ~ %s", tt.Path, tt.Pattern), func(t *testing.T) {
			t.Parallel()

			s, err := pattern.NewContent(tt.Pattern)
			require.NoError(t, err)
			require.NotNil(t, s)

			f, err := file.New("", tt.Path)
			require.NoError(t, err)
			ok, err := s.Match(ctx, f)

			require.NoError(t, err)

			if tt.Expected {
				assert.True(t, ok, "%q ~ %q", tt.Path, tt.Pattern)

				return
			}

			assert.False(t, ok, "%q !~ %q", tt.Path, tt.Pattern)
		})
	}
}
