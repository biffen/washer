package command

import (
	"context"
	"fmt"

	"gitlab.com/biffen/go-applause"
	"gitlab.com/biffen/washer/internal"
	"gitlab.com/biffen/washer/internal/errors"
	"gopkg.in/yaml.v3"
)

var _ Command = (*Spec)(nil)

type Spec struct {
	Dump *Dump

	File bool `option:"file|f"`
}

func NewSpec(
	ctx context.Context,
	l *Dump,
	parser *applause.Parser,
	args ...string,
) (*Spec, error) {
	s := &Spec{
		Dump: l,
	}

	parser.PassThrough = false
	if err := parser.Add(s); err != nil {
		panic(errors.Internal(err))
	}

	operands, err := parser.Parse(ctx, args)
	if err != nil {
		return nil, err
	}

	if len(operands) > 0 {
		return nil,
			fmt.Errorf("%w: extra arguments %q", errors.ErrUser, operands)
	}

	return s, nil
}

func (*Spec) Close() error {
	return nil
}

func (s *Spec) Run(ctx context.Context) error {
	w, err := s.Dump.Writer(ctx, internal.GetWasher(ctx).OriginalDirectory)
	if err != nil {
		return err
	}

	if s.File {
		var path string

		path, err = internal.GetWasher(ctx).SpecPath(ctx)
		if err != nil {
			return err
		}

		_, err = fmt.Fprintln(w, path)

		return err
	}

	spec, err := internal.GetWasher(ctx).Spec(ctx)
	if err != nil {
		return err
	}

	if err = yaml.NewEncoder(w).Encode(spec); err != nil {
		return fmt.Errorf("failed to encode YAML: %w", err)
	}

	return nil
}
