package command

import (
	"context"
	"fmt"
	"strings"

	errör "gitlab.com/biffen/error"
	"gitlab.com/biffen/go-applause"
	"gitlab.com/biffen/washer/internal"
	"gitlab.com/biffen/washer/internal/errors"
	"gitlab.com/biffen/washer/internal/file"
	"gitlab.com/biffen/washer/internal/spec"
	"gitlab.com/biffen/washer/internal/task"
	"gitlab.com/biffen/washer/internal/util"
	"golang.org/x/exp/slices"
	"golang.org/x/sync/errgroup"
)

var _ Command = (*Dump)(nil)

const (
	FS          = "\t"
	DumpCommand = "dump"
)

type Dump struct {
	Output

	Paths      []string
	Tools      []string `option:"tool|t"`
	Separator  string   `option:"separator|F"`
	SubCommand Command
	Null       bool `option:"null-terminated|z"`
	Sort       bool `option:"sort"`
}

func NewDump(
	ctx context.Context,
	parser *applause.Parser,
	args ...string,
) (*Dump, error) {
	d := &Dump{
		Separator: FS,
	}

	parser.PassThrough = true
	if err := parser.Add(d); err != nil {
		panic(errors.Internal(err))
	}

	operands, err := parser.Parse(ctx, args)
	if err != nil {
		return nil, err
	}

	command, operands := SubCommand(operands...)

	if command == nil {
		d.Paths = operands
	} else {
		switch *command {
		case "files":
			d.SubCommand, err = NewFiles(ctx, d, parser, operands...)

		case "specification", "spec":
			d.SubCommand, err = NewSpec(ctx, d, parser, operands...)

		case "tools":
			d.SubCommand, err = NewTools(ctx, d, parser, operands...)

		default:
			return nil,
				fmt.Errorf("%w: unknown dump %q", errors.ErrUser, *command)
		}

		if err != nil {
			return nil, err
		}
	}

	return d, nil
}

func (d *Dump) Close() error {
	return errör.CloseAll(d.SubCommand)
}

func (d *Dump) Generate(context.Context) (elements [][]string, err error) {
	return
}

func (d *Dump) Run(ctx context.Context) error {
	if d.SubCommand == nil {
		return d.run(ctx, func(
			_ context.Context,
			tool *spec.Tool,
			f *file.File,
		) (record []string, err error) {
			if f == nil {
				return []string{tool.String()}, nil
			}

			return []string{tool.String(), f.Path}, nil
		})
	}

	return d.SubCommand.Run(ctx)
}

func (d *Dump) run(
	ctx context.Context,
	generator Generator,
) error {
	w, err := d.Output.Writer(ctx, internal.GetWasher(ctx).OriginalDirectory)
	if err != nil {
		return err
	}

	s, err := internal.GetWasher(ctx).Spec(ctx)
	if err != nil {
		return err
	}

	tools, err := s.GetTools(ctx, nil, nil)
	if err != nil {
		return err
	}

	var eg errgroup.Group

	tasks := make(chan *task.Task)

	transformers, err := internal.GetWasher(ctx).GetFiles(ctx, "")
	if err != nil {
		return err
	}

	sc := util.Chain[*file.File]{
		ChanSize:     1,
		Transformers: transformers,
	}
	wd := internal.GetWasher(ctx).Directory

	explicitFiles, err := util.TransformErr(
		ctx,
		func(path string) (*file.File, error) {
			return file.New(wd, path)
		},
		d.Paths...)
	if err != nil {
		return err
	}

	filesChan, f := sc.Run(ctx, explicitFiles...)

	files, err := util.ChanToSlice(ctx, filesChan)
	if err != nil {
		return err
	}

	if err := f(); err != nil {
		return err
	}

	eg.Go(func() error {
		defer close(tasks)

		return task.Make(ctx, task.OperationCheck, tools, files, tasks)
	})

	var data [][]string

	eg.Go(func() error {
		for task := range tasks {
			record, err := generator(ctx, task.Tool, task.File)
			switch {
			case err != nil:
				return err

			case record != nil:
			OUTER:
				for _, r := range data {
					if len(r) != len(record) {
						continue
					}

					for i, f := range r {
						if f != record[i] {
							continue OUTER
						}
					}

					return nil
				}

				data = append(data, record)
			}
		}

		return nil
	})

	if err := eg.Wait(); err != nil {
		return err
	}

	records := util.Transform(ctx, func(datum []string) string {
		return strings.Join(datum, d.Separator)
	}, data...)

	if d.Sort {
		slices.Sort(records)
	}

	var rs rune
	if !d.Null {
		rs = '\n'
	}

	for _, r := range records {
		if _, err := fmt.Fprintf(w, "%s%c", r, rs); err != nil {
			return err
		}
	}

	return nil
}

type Generator func(
	ctx context.Context,
	tool *spec.Tool,
	f *file.File,
) (record []string, err error)
