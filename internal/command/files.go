package command

import (
	"context"

	"gitlab.com/biffen/go-applause"
	"gitlab.com/biffen/washer/internal/errors"
	"gitlab.com/biffen/washer/internal/file"
	"gitlab.com/biffen/washer/internal/spec"
)

var _ Command = (*Files)(nil)

type Files struct {
	dump *Dump
}

func NewFiles(
	ctx context.Context,
	d *Dump,
	parser *applause.Parser,
	args ...string,
) (*Files, error) {
	f := &Files{
		dump: d,
	}

	parser.PassThrough = false
	if err := parser.Add(f); err != nil {
		panic(errors.Internal(err))
	}

	operands, err := parser.Parse(ctx, args)
	if err != nil {
		return nil, err
	}

	f.dump.Paths = operands

	return f, nil
}

func (f *Files) Close() error {
	return nil
}

func (f *Files) Run(ctx context.Context) error {
	return f.dump.run(ctx, func(
		_ context.Context,
		_ *spec.Tool,
		f *file.File,
	) (record []string, err error) {
		return []string{f.Path}, nil
	})
}
