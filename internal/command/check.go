package command

import (
	"context"
	"io"
	"os"
	"strings"

	errör "gitlab.com/biffen/error"
	"gitlab.com/biffen/go-applause"
	"gitlab.com/biffen/washer/internal"
	"gitlab.com/biffen/washer/internal/errors"
	"gitlab.com/biffen/washer/internal/format"
	"gitlab.com/biffen/washer/internal/task"
	"golang.org/x/term"
)

var _ Command = (*Check)(nil)

const (
	CheckCommand = "check"
)

// Check is the check sub-command.
type Check struct {
	CheckFix

	closers []io.Closer
	outputs map[string]string
}

func NewCheck(
	ctx context.Context,
	parser *applause.Parser,
	args ...string,
) (c *Check, err error) {
	c = &Check{
		CheckFix: CheckFix{
			Backup: true,

			hc: internal.HarnessConfig{
				GitStash:  true,
				Lock:      false,
				Operation: task.OperationCheck,
			},
		},
	}

	c.CheckFix.setupFormat = c.setupFormat

	parser.PassThrough = false
	if err = parser.Add(
		c,
		"output|o", c.addOutput,
	); err != nil {
		panic(errors.Internal(err))
	}

	c.Paths, err = parser.Parse(ctx, args)
	if err != nil {
		return
	}

	return
}

func (check *Check) Close() (err error) {
	return errör.CloseAll(check.closers...)
}

func (check *Check) addOutput(str string) {
	if check.outputs == nil {
		check.outputs = make(map[string]string, 1)
	}

	parts := strings.SplitN(str, "=", 1+1)
	parts = append(parts, "")
	check.outputs[parts[0]] = parts[1]
}

func (check *Check) setupFormat(
	ctx context.Context,
	hc *internal.HarnessConfig,
) error {
	var (
		tty     = term.IsTerminal(int(os.Stdout.Fd()))
		outputs = check.outputs
		washer  = internal.GetWasher(ctx)
	)

	if len(outputs) == 0 && washer.Verbosity >= 0 {
		outputs = map[string]string{"-": ""}
	}

	for k, v := range outputs {
		output := &Output{
			Output: k,
		}
		check.closers = append(check.closers, output)

		w, err := output.Writer(
			ctx,
			washer.OriginalDirectory,
		)
		if err != nil {
			return err
		}

		f, err := format.New(v, format.Config{
			ASCII:     !tty,
			Colour:    tty,
			Verbosity: washer.Verbosity,
			Writer:    w,
		})
		if err != nil {
			return err
		}

		if f != nil {
			hc.Observers = append(hc.Observers, f)
		}

		path, err := output.Path()
		if err != nil {
			return err
		}

		if path != "" {
			hc.Exclude = append(hc.Exclude, path)
		}
	}

	return nil
}
