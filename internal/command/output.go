package command

import (
	"context"
	"fmt"
	"io"
	"sync"

	errör "gitlab.com/biffen/error"
	"gitlab.com/biffen/washer/internal/data"
	"gitlab.com/biffen/washer/internal/util"
)

var (
	_ io.Closer   = (*Output)(nil)
	_ util.Pather = (*Output)(nil)
)

type Output struct {
	Output string `option:"output|o"`

	mtx    sync.Mutex
	writer io.WriteCloser
}

func (w *Output) Close() error {
	w.mtx.Lock()
	defer w.mtx.Unlock()

	return errör.CloseAll(w.writer)
}

func (w *Output) Path() (path string, err error) {
	if p, ok := w.writer.(util.Pather); ok {
		path, err = p.Path()
	}

	return
}

func (w *Output) Writer(ctx context.Context, wd string) (io.Writer, error) {
	w.mtx.Lock()
	defer w.mtx.Unlock()

	if w.writer == nil {
		if w.Output == "" {
			w.Output = "-"
		}

		var err error
		if w.writer, err = data.NewWriter(
			ctx,
			w.Output,
			data.OptionBackup("washer"),
			data.OptionMkdir(true),
			data.OptionWorkingDirectory(wd),
		); err != nil {
			return nil,
				fmt.Errorf("can’t write to output %q: %w", w.Output, err)
		}
	}

	return w.writer, nil
}
