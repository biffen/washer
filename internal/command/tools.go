package command

import (
	"context"

	"gitlab.com/biffen/go-applause"
	"gitlab.com/biffen/washer/internal/errors"
	"gitlab.com/biffen/washer/internal/file"
	"gitlab.com/biffen/washer/internal/spec"
)

// var _ Command = (*Tools)(nil).

type Tools struct {
	dump *Dump
}

func NewTools(
	ctx context.Context,
	d *Dump,
	parser *applause.Parser,
	args ...string,
) (*Tools, error) {
	t := &Tools{
		dump: d,
	}

	parser.PassThrough = false
	if err := parser.Add(t); err != nil {
		panic(errors.Internal(err))
	}

	operands, err := parser.Parse(ctx, args)
	if err != nil {
		return nil, err
	}

	t.dump.Paths = operands

	return t, nil
}

func (t *Tools) Close() error {
	return nil
}

func (t *Tools) Run(ctx context.Context) error {
	return t.dump.run(ctx, func(
		_ context.Context,
		tool *spec.Tool,
		_ *file.File,
	) (record []string, err error) {
		return []string{tool.String()}, nil
	})
}
