package command

import (
	"context"
	"io"
	"strings"
)

func SubCommand(args ...string) (command *string, operands []string) {
	for i, arg := range args {
		if strings.HasPrefix(arg, "-") {
			continue
		}

		tmp := arg
		command = &tmp

		operands = args[:i]
		if i+1 < len(args) {
			operands = append(operands, args[i+1:]...)
		}

		return
	}

	operands = args

	return
}

type Command interface {
	io.Closer

	Run(ctx context.Context) error
}
