package command_test

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/biffen/washer/internal/command"
)

func TestSubCommand(t *testing.T) {
	t.Parallel()

	for _, tt := range [...]struct {
		Args     []string
		Command  string
		Operands []string
	}{
		{[]string{}, "", []string{}},
		{[]string{"foo"}, "foo", []string{}},
		{[]string{"foo", "bar"}, "foo", []string{"bar"}},
		{[]string{"--foo", "bar"}, "bar", []string{"--foo"}},
		{[]string{"--foo=bar"}, "", []string{"--foo=bar"}},
	} {
		tt := tt

		t.Run(fmt.Sprintf("%q", tt.Args), func(t *testing.T) {
			t.Parallel()

			if tt.Args == nil {
				tt.Args = []string{}
			}

			cmd, operands := command.SubCommand(tt.Args...)

			if operands == nil {
				operands = []string{}
			}

			switch {
			case tt.Command == "":
				assert.Nil(t, cmd, "no command")

			case assert.NotNil(t, cmd):
				assert.Equal(t, tt.Command, *cmd, "the command")
			}

			assert.Equal(t, tt.Operands, operands, "the operands")
		})
	}
}
