package internal

import (
	"context"
	goerrors "errors"
	"fmt"
	"io/fs"
	"os"
	"os/exec"
	"path/filepath"
	"sync"

	"gitlab.com/biffen/washer/internal/debug"
	"gitlab.com/biffen/washer/internal/errors"
	"gitlab.com/biffen/washer/internal/file"
	"gitlab.com/biffen/washer/internal/git"
	"gitlab.com/biffen/washer/internal/spec"
	"gitlab.com/biffen/washer/internal/util"
)

const (
	name                        = "washer"
	washerContextKey contextKey = iota
)

func WithWasher(ctx context.Context, w *Washer) context.Context {
	return context.WithValue(ctx, washerContextKey, w)
}

type Pair struct {
	Tool *spec.Tool
	File *file.File
}

type Washer struct {
	Directory, OriginalDirectory string
	Specification                string
	Verbosity                    int

	spec        *spec.Spec
	specMtx     sync.Mutex
	specPath    string
	specPathMtx sync.Mutex
}

func GetWasher(ctx context.Context) *Washer {
	w, ok := ctx.Value(washerContextKey).(*Washer)

	if !ok {
		panic(errors.Internal(nil))
	}

	return w
}

func (w *Washer) ChDir(context.Context) (err error) {
	if w.Directory == "" {
		return
	}

	if w.OriginalDirectory, err = os.Getwd(); err != nil {
		return
	}

	if w.Directory, err = filepath.Abs(w.Directory); err != nil {
		return
	}

	if err = os.Chdir(w.Directory); err != nil {
		return
	}

	w.Logf("washer: Entering directory `%s'", w.Directory)

	return
}

func (w *Washer) FindFiles(
	ctx context.Context,
	in <-chan *file.File,
	out chan<- *file.File,
) error {
	s, err := w.Spec(ctx)
	if err != nil {
		return err
	}

	for f := range in {
		files, err := w.findFiles(ctx, f, func(f *file.File) (bool, error) {
			exclude, match, err := s.Exclude.Match(ctx, f)
			if err != nil {
				return false,
					fmt.Errorf("failed check %q for exclusion: %w", f, err)
			}

			if exclude {
				debug.Printf("Exclude %q (matches %s)", f, match)
			}

			return !exclude, nil
		})
		if err != nil {
			return err
		}

		if err := util.SliceToChan(ctx, files, out); err != nil {
			return err
		}
	}

	return nil
}

func (w *Washer) GetFiles(
	ctx context.Context,
	gitOption GitOption,
) (transformers []util.Transformer[*file.File], err error) {
	repo, err := git.New(ctx, w.Directory)

	switch {
	case err == nil:

	case goerrors.Is(err, exec.ErrNotFound):
		if gitOption != "" {
			return nil, err
		}

	default:
		return nil, err
	}

	switch gitOption {
	case "":
		if repo == nil {
			defaultPath, err := file.New(w.Directory, ".")
			if err != nil {
				return nil, fmt.Errorf("failed to get working directory: %w", err)
			}

			transformers = append(transformers, DefaultPath(defaultPath))

			break
		}

		fallthrough

	case GitOptionAll:
		transformers = append(
			transformers,
			gitFiles(
				func(ctx context.Context, paths []string) ([]string, error) {
					return repo.Files(ctx, true, paths...)
				},
			),
		)

	case GitOptionChanged:
		transformers = append(
			transformers,
			gitFiles(
				func(ctx context.Context, paths []string) ([]string, error) {
					return repo.DiffFiles(ctx, git.DiffChanged, paths...)
				},
			),
		)

	case GitOptionStaged:
		transformers = append(
			transformers,
			gitFiles(
				func(ctx context.Context, paths []string) ([]string, error) {
					return repo.DiffFiles(ctx, git.DiffStaged, paths...)
				},
			),
		)

	case GitOptionTracked:
		transformers = append(
			transformers,
			gitFiles(
				func(ctx context.Context, paths []string) ([]string, error) {
					return repo.Files(ctx, false, paths...)
				},
			),
		)

	default:
		panic(errors.Internal(nil))
	}

	transformers = append(transformers, GetWasher(ctx).FindFiles)

	if repo != nil {
		transformers = append(transformers, removeIgnored(repo))
	}

	return
}

func (*Washer) Logf(format string, args ...interface{}) {
	fmt.Fprintf(os.Stderr, "washer: "+format+"\n", args...)
}

func (w *Washer) Spec(ctx context.Context) (s *spec.Spec, err error) {
	w.specMtx.Lock()
	defer w.specMtx.Unlock()

	if w.spec == nil {
		var path string

		path, err = w.SpecPath(ctx)
		if err != nil {
			return
		}

		s, err = spec.New(
			ctx,
			path,
			spec.DefaultSource(w.OriginalDirectory),
		)
		if err != nil {
			return
		}

		w.spec = s
	}

	s = w.spec

	return
}

func (w *Washer) SpecPath(ctx context.Context) (path string, err error) {
	w.specPathMtx.Lock()
	defer w.specPathMtx.Unlock()

	defer func() {
		path = w.specPath
	}()

	if w.specPath == "" {
		if w.Specification != "" {
			w.specPath = w.Specification

			return
		}

		if env := os.Getenv(EnvWasherSpecification); env != "" {
			w.specPath = env

			return
		}

		var (
			files []fs.DirEntry
			wd    string
		)

		if wd, err = os.Getwd(); err != nil {
			return
		}

		if wd, err = filepath.Abs(wd); err != nil {
			return
		}

		files, err = os.ReadDir(".")
		if err != nil {
			err = fmt.Errorf(
				"failed to list files in directory %q: %w",
				wd,
				err,
			)

			return
		}

		candidates := util.Transform(
			ctx,
			func(info fs.DirEntry) string {
				return info.Name()
			},
			util.Filter(spec.LooksLikeSpecFile, files...)...,
		)

		if len(candidates) == 0 {
			err = fmt.Errorf(
				"%w: no specification file in %q",
				errors.ErrUser,
				wd,
			)

			return
		}

		if len(candidates) > 1 {
			err = fmt.Errorf(
				"%w: multiple specification files: %q",
				errors.ErrUser,
				candidates,
			)

			return
		}

		w.specPath = filepath.Join(wd, candidates[0])
	}

	return
}

func (w *Washer) findFiles(
	//nolint:unparam // Not true!
	ctx context.Context,
	f *file.File,
	include func(*file.File) (bool, error),
) (files []*file.File, err error) {
	if exists, err := f.Exists(); err != nil {
		return nil, err
	} else if !exists {
		return nil, nil
	}

	if f.IsDir() {
		if f.Path != "" {
			var ok bool
			if ok, err = include(f); !ok {
				return
			} else if err != nil {
				return
			}
		}

		var children []fs.DirEntry

		if children, err = os.ReadDir(f.Abs()); err != nil {
			return
		}

		for _, child := range children {
			var (
				ff  []*file.File
				fff *file.File
			)

			if fff, err = file.New(
				f.WD,
				filepath.Join(f.Path, child.Name()),
			); err != nil {
				return
			}

			ff, err = w.findFiles(
				ctx,
				fff,
				include,
			)
			if err != nil {
				return
			}

			files = append(files, ff...)
		}

		return
	}

	var ok bool
	if ok, err = include(f); !ok {
		return
	} else if err != nil {
		return
	}

	files = []*file.File{f}

	debug.Printf("Include file %q", f)

	return
}

type contextKey int
