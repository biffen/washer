package git

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	"gitlab.com/biffen/washer/internal/util"
)

const (
	DiffChanged  DiffMode = "HEAD"
	DiffStaged   DiffMode = "--cached"
	DiffUnstaged DiffMode = ""
	git                   = "git"
)

var _ fmt.Stringer = (*Repository)(nil)

func Directory(ctx context.Context, dir string) (string, error) {
	var out strings.Builder

	if dir == "" {
		dir = "."
	}

	cmd := exec.CommandContext(ctx, git, "rev-parse", "--show-toplevel")
	cmd.Dir = dir

	cmd.Stdout = &out

	if err := cmd.Run(); err != nil {
		return "",
			fmt.Errorf("failed to get Git directory for %q: %w", dir, err)
	}

	return strings.TrimRight(out.String(), "\n"), nil
}

type DiffMode string

type Repository string

func New(ctx context.Context, path string) (*Repository, error) {
	path, err := filepath.Abs(path)
	if err != nil {
		return nil, err
	}

	dir, err := Directory(ctx, path)
	if err != nil {
		return nil, err
	}

	r := Repository(dir)

	return &r, nil
}

func (r *Repository) Command(
	ctx context.Context,
	args ...string,
) (output []byte, err error) {
	cmd := exec.CommandContext(ctx, git, args...)

	cmd.Env = append(os.Environ(),
		"GIT_PAGER=",
		"PAGER=",
	)

	var out bytes.Buffer
	cmd.Stdout = &out

	if err = cmd.Run(); err != nil {
		err = fmt.Errorf("git error: %w", err)
	}

	output = out.Bytes()

	return
}

func (r *Repository) DiffFiles(
	ctx context.Context,
	mode DiffMode,
	paths ...string,
) (files []string, err error) {
	cmd := []string{
		"diff",
		"--colo" + "r=never",
		"--diff-filter=d",
		"--ignore-submodules=all",
		"--name-only",
		"-z",
	}

	switch mode {
	case DiffChanged, DiffStaged:
		cmd = append(cmd, string(mode))

	case DiffUnstaged:

	default:
		panic(fmt.Sprintf("unknown DiffMode %q", mode))
	}

	cmd = append(cmd, "--")
	cmd = append(cmd, paths...)

	files, err = r.z(ctx, cmd...)
	if err != nil {
		return nil, fmt.Errorf("diff failed: %w", err)
	}

	return
}

func (r *Repository) Files(
	ctx context.Context,
	untracked bool,
	paths ...string,
) (files []string, err error) {
	cmd := []string{
		"ls-files",
		"--cached",
		"--exclude-standard",
		"-z",
	}

	if untracked {
		cmd = append(cmd, "--others")
	}

	cmd = append(cmd, "--")
	cmd = append(cmd, paths...)

	files, err = r.z(ctx, cmd...)
	if err != nil {
		return nil, fmt.Errorf("ls-files failed: %w", err)
	}

	return
}

func (r *Repository) Ignored(ctx context.Context, path string) bool {
	_, err := r.Command(ctx, "check-ignore", "--quiet", path)

	// TODO doesn’t handle submodules nicely

	var exitError *exec.ExitError

	switch {
	case err == nil:
		return true

	case errors.As(err, &exitError):
		if exitError.ProcessState.ExitCode() == 1 {
			return false
		}
	}

	// TODO log err

	return false
}

func (r *Repository) String() string {
	return string(*r)
}

func (r *Repository) z(ctx context.Context, cmd ...string) ([]string, error) {
	out, err := r.Command(ctx, cmd...)
	if err != nil {
		return nil, err
	}

	return util.Transform(ctx, func(b []byte) string {
		return string(b)
	},
		util.Filter(util.NonEmpty[byte], bytes.Split(out, []byte{0})...)...,
	), nil
}
