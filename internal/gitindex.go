package internal

import (
	"context"
	"fmt"
	"sync"

	errör "gitlab.com/biffen/error"
	"gitlab.com/biffen/washer/internal/backup"
	"gitlab.com/biffen/washer/internal/debug"
	"gitlab.com/biffen/washer/internal/file"
	"gitlab.com/biffen/washer/internal/git"
	"gitlab.com/biffen/washer/internal/spec"
	"gitlab.com/biffen/washer/internal/task"
)

var _ Observer = (*GitIndex)(nil)

type GitIndex struct {
	mtx     sync.Mutex
	repo    git.Repository
	backups []*backup.Backup
}

func (gi *GitIndex) AfterTask(context.Context, *task.Result) error {
	return nil
}

func (gi *GitIndex) BeforeTask(
	context.Context,
	*spec.Tool,
	*file.File,
) error {
	return nil
}

func (gi *GitIndex) Close() (err error) {
	gi.mtx.Lock()
	defer gi.mtx.Unlock()

	for _, b := range gi.backups {
		err = errör.Composite(err, b.Restore())
	}

	return
}

func (gi *GitIndex) Open(ctx context.Context) error {
	files, err := gi.repo.DiffFiles(ctx, git.DiffUnstaged)
	if err != nil {
		return fmt.Errorf("failed to list staged files: %w", err)
	}

	debug.Printf("%d file(s) are have un-staged changes", len(files))

	gi.mtx.Lock()
	defer gi.mtx.Unlock()

	for _, file := range files {
		b, err := backup.New(ctx, file, "washer-unstaged")
		if err != nil {
			return fmt.Errorf(
				"failed to backup un-staged changes of %q: %w",
				file,
				err,
			)
		}

		debug.Printf("Backed up un-staged changes in %q into %q", file, b)

		if _, err = gi.repo.Command(ctx, "checkout", "--", file); err != nil {
			return fmt.Errorf(
				"failed to revert un-staged changes of %q: %w",
				file,
				err,
			)
		}

		gi.backups = append(gi.backups, b)
	}

	return nil
}
