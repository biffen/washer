package parse

import (
	"context"
	"encoding/xml"
	"fmt"
	"io"
	"strings"

	"gitlab.com/biffen/washer/internal/junit"
	"gitlab.com/biffen/washer/internal/location"
	"gitlab.com/biffen/washer/internal/util"
)

var _ parser = JUnit

func JUnit(
	ctx context.Context,
	stdout, _ io.Reader,
) ([]location.Error, error) {
	var suites junit.TestSuites
	if err := xml.NewDecoder(stdout).
		Decode(&suites); err != nil {
		return nil, fmt.Errorf("failed to decode JUnit XML: %w", err)
	}

	return util.Transform(
		ctx,
		func(c *junit.TestCase) location.Error {
			return location.Error{
				Message: strings.TrimSpace(
					c.Failure.Message + "\n" + c.Failure.Description,
				),
			}
		},
		util.Filter(
			func(c *junit.TestCase) bool {
				return c.Failure != nil && c.Skipped == nil
			},
			util.Concatenate(
				util.Transform(
					ctx,
					func(suite *junit.TestSuite) []*junit.TestCase {
						return suite.TestCases
					},
					suites.TestSuites...,
				)...,
			)...,
		)...,
	), nil
}
