package spec

import (
	"context"
	"fmt"
	"io"
	"io/fs"
	"path/filepath"
	"regexp"

	"github.com/imdario/mergo"
	"gitlab.com/biffen/washer/internal/data"
	"gitlab.com/biffen/washer/internal/errors"
	"gitlab.com/biffen/washer/internal/pattern"
	"gitlab.com/biffen/washer/internal/util"
	"gopkg.in/yaml.v3"
)

var specFilePattern = regexp.MustCompile(`(?i)^\.?washer\.ya?ml$`)

func LooksLikeSpecFile(info fs.DirEntry) bool {
	return specFilePattern.MatchString(info.Name())
}

func ParseSpec(
	ctx context.Context,
	content []byte,
	s *Spec,
	source Source,
) error {
	if err := yaml.Unmarshal(content, s); err != nil {
		return fmt.Errorf("error parsing specification: %w", err)
	}

	return s.Flatten(ctx, source)
}

type Source func(ctx context.Context, location string) ([]byte, error)

func DefaultSource(workingDir string) Source {
	return func(ctx context.Context, location string) ([]byte, error) {
		r, err := data.NewReader(
			ctx,
			location,
			data.OptionWorkingDirectory(workingDir),
		)
		if err != nil {
			return nil, fmt.Errorf(
				"can’t read from %q (%q): %w",
				location,
				workingDir,
				err,
			)
		}

		b, err := io.ReadAll(r)
		if err != nil {
			return nil, fmt.Errorf("can’t read from %q: %w", location, err)
		}

		if err = r.Close(); err != nil {
			return nil, fmt.Errorf("can’t close %q: %w", location, err)
		}

		return b, nil
	}
}

type Spec struct {
	Location string `yaml:"-"`

	Exclude *pattern.Set `yaml:"exclude,omitempty"`
	Extends []string     `yaml:"extends,omitempty"`
	Tools   *ToolSet     `yaml:"tools,omitempty"`
	// GlobalTools map[string]GlobalTool `yaml:"global"`

	wd string
}

func New(
	ctx context.Context,
	location string,
	source Source,
) (*Spec, error) {
	b, err := source(ctx, location)
	if err != nil {
		return nil, err
	}

	s := &Spec{
		Location: location,
	}

	if path, errr := data.AsPath(s.Location); errr == nil && path != "" {
		s.wd = filepath.Dir(path)
	}

	if err := ParseSpec(ctx, b, s, source); err != nil {
		return nil, err
	}

	return s, nil
}

func (s *Spec) Flatten(ctx context.Context, source Source) error {
	for _, location := range s.Extends {
		if path, e := data.AsPath(location); e == nil &&
			path != "" &&
			!filepath.IsAbs(path) {
			location = filepath.Join(s.wd, path)
		}

		extends, err := New(ctx, location, source)
		if err != nil {
			return fmt.Errorf(
				"error parsing extended specification %q: %w",
				location,
				err,
			)
		}

		if err = mergo.Merge(
			s,
			extends,
			mergo.WithTransformers(util.MergerTransformer{}),
		); err != nil {
			panic(
				errors.Internal(
					fmt.Errorf("error merging specifications: %w", err),
				),
			)
		}
	}

	s.Extends = nil

	return nil
}

func (s *Spec) GetTools(
	ctx context.Context,
	include, exclude []string,
) (tools *ToolSet, err error) {
	if len(include) == 0 {
		tools = new(ToolSet)
		tools.Insert(ctx, util.Filter(func(t *Tool) bool {
			return !t.deleted
		}, s.Tools.Slice()...)...)
	} else {
		tools = new(ToolSet)

		t, err := util.TransformErr(ctx, func(name string) (*Tool, error) {
			tool, ok, err := s.Tools.Load(ctx, name)
			switch {
			case err != nil:
				return nil, err

			case !ok:
				return nil, fmt.Errorf("%w: unknown tool %q", errors.ErrUser, name)

			default:
				return tool, nil
			}
		}, include...)
		if err != nil {
			return nil, err
		}

		tools.Insert(ctx, t...)
	}

	if len(exclude) > 0 {
		noTool := new(ToolSet)
		noTool.Insert(
			ctx,
			util.Filter(func(tool *Tool) bool {
				return tool != nil
			},
				util.Transform(
					ctx,
					func(name string) *Tool {
						tool, _, _ := s.Tools.Load(ctx, name)

						return tool
					},
					exclude...,
				)...,
			)...,
		)

		left, _, _ := tools.IntDiff(ctx, &noTool.SortedSet)
		tools = &ToolSet{left}
	}

	return tools, nil
}
