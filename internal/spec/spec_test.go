package spec_test

import (
	"context"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/biffen/washer/internal/pattern"
	"gitlab.com/biffen/washer/internal/spec"
	"gitlab.com/biffen/washer/internal/util"
)

func TestFlatten(t *testing.T) {
	t.Parallel()

	var (
		ctx    = context.Background()
		source = func(ctx context.Context, location string) ([]byte, error) {
			switch location {
			case "a":
				return []byte(`
extends:
  - b
  - c

exclude:
  - '*.a'

tools:
  b:
    check:
      - a
      - --check

  e:
    check:
      - should
      - be
      - deleted
`), nil
			case "b":
				return []byte(`
extends:
  - d

exclude:
  - '*.b'

tools:
  b:
    check:
      - b
      - --check
    fix:
      - b
      - --fix
`), nil
			case "c":
				return []byte(`

exclude:
  - '*.c'

tools:
  c: {}
`), nil
			case "d":
				return []byte(`

exclude:
  - '*.d'

tools:
  d: {}
  e: ~
`), nil
			default:
				return nil, fmt.Errorf("%q not found", location)
			}
		}
	)

	s, err := spec.New(ctx, "nope", source)
	assert.Error(t, err)
	assert.Regexp(t, `\bnope\b.*\bnot found`, err.Error())
	assert.Nil(t, s)

	s, err = spec.New(ctx, "a", source)
	require.NoError(t, err)
	require.NotNil(t, s)

	for _, tool := range [...]string{"b", "c", "d"} {
		tool, ok, err2 := s.Tools.Load(ctx, tool)
		assert.NoError(t, err2)
		assert.NotNil(t, tool)
		assert.True(t, ok, "tool %q exists", tool)
	}

	toolB, ok, err := s.Tools.Load(ctx, "b")
	assert.NoError(t, err)
	assert.NotNil(t, toolB)
	assert.True(t, ok)

	toolE, ok, err := s.Tools.Load(ctx, "e")
	assert.NoError(t, err)
	assert.Nil(t, toolE)
	assert.False(t, ok)

	if assert.NotNil(t, toolB) {
		if assert.NotNil(t, toolB.CheckCmd) {
			assert.Equal(t, []string{"a", "--check"}, toolB.CheckCmd.Command)
		}

		if assert.NotNil(t, toolB.FixCmd) {
			assert.Equal(t, []string{"b", "--fix"}, toolB.FixCmd.Command)
		}
	}

	assert.ElementsMatch(t, []string{
		"*.a",
		"*.b",
		"*.c",
		"*.d",
	}, util.Transform(ctx, func(p *pattern.Pattern) string {
		return p.String
	}, s.Exclude.Slice()...))
}

func TestNewSpec(t *testing.T) {
	t.Parallel()

	for _, path := range [...]string{
		"washer.yaml",
		// "docs/example-specification.yaml",
	} {
		path := path

		t.Run(path, func(t *testing.T) {
			t.Parallel()

			config, err := spec.New(
				context.Background(),
				path,
				spec.DefaultSource("../.."),
			)
			require.NoError(t, err)
			assert.NotNil(t, config)
		})
	}
}
