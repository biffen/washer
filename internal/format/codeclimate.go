package format

import (
	"context"
	"encoding/json"
	"fmt"
	"sync"

	"gitlab.com/biffen/washer/internal/codeclimate"
	"gitlab.com/biffen/washer/internal/file"
	"gitlab.com/biffen/washer/internal/spec"
	"gitlab.com/biffen/washer/internal/task"
)

var _ Format = (*Codeclimate)(nil)

const (
	FormatCodeclimate = "codeclimate"
	initialCapacity   = 0x100
)

type Codeclimate struct {
	Config

	mtx    sync.Mutex
	issues []*codeclimate.Issue
}

func NewCodeclimate(config Config) *Codeclimate {
	return &Codeclimate{
		Config: config,
	}
}

func (c *Codeclimate) AfterTask(ctx context.Context, res *task.Result) error {
	var issues []*codeclimate.Issue

	switch {
	case res.Error != nil:
		issue, err := codeclimate.NewIssue(
			res.Tool.String(),
			fmt.Sprintf("%s failed: %v", res.FullCommand(), res.Error),
			[]codeclimate.Category{codeclimate.Style},
			codeclimate.Location{
				Path: res.File.Path,
				Lines: codeclimate.Lines{
					Begin: 1,
					End:   1,
				},
			},
		)
		if err != nil {
			return err
		}

		issue.Severity = codeclimate.Critical

		issues = []*codeclimate.Issue{issue}

	case res.Success:
		return nil

	default:
		errors, err := res.Errors(ctx)
		if err != nil {
			return err
		}

		for _, error := range errors {
			location := codeclimate.Location{
				Path: error.Location.File,
				Lines: codeclimate.Lines{
					Begin: error.Location.Line,
					End:   error.Location.Line2,
				},
			}

			if location.Lines.Begin == 0 {
				location.Lines.Begin = 1
			}

			issue, err := codeclimate.NewIssue(
				res.Tool.String(),
				error.Message,
				[]codeclimate.Category{codeclimate.Style},
				location,
			)
			if err != nil {
				return err
			}

			issue.Severity = codeclimate.Critical

			issues = append(issues, issue)
		}
	}

	c.mtx.Lock()
	defer c.mtx.Unlock()

	c.issues = append(c.issues, issues...)

	return nil
}

func (*Codeclimate) BeforeTask(context.Context, *spec.Tool, *file.File) error {
	return nil
}

func (c *Codeclimate) Close() (err error) {
	c.mtx.Lock()
	defer c.mtx.Unlock()

	e := json.NewEncoder(c.Writer)
	e.SetIndent("", indentation)

	return e.Encode(c.issues)
}

func (c *Codeclimate) Open(context.Context) error {
	c.mtx.Lock()
	defer c.mtx.Unlock()

	c.issues = make([]*codeclimate.Issue, 0, initialCapacity)

	return nil
}
