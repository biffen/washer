package format

import (
	"context"
	"fmt"
	"strings"
	"sync"

	"github.com/logrusorgru/aurora/v3"
	errör "gitlab.com/biffen/error"
	"gitlab.com/biffen/washer/internal/file"
	"gitlab.com/biffen/washer/internal/spec"
	"gitlab.com/biffen/washer/internal/task"
	"gitlab.com/biffen/washer/internal/util"
	"golang.org/x/exp/maps"
)

var _ Format = (*Text)(nil)

const (
	ASCIIFail      = '-'
	ASCIIOK        = '+'
	ASCIISeparator = '/'
	Fail           = '❌'
	LISTMAX        = 8
	OK             = '✓'
	Separator      = '•'
)

const FormatText = "text"

func failed[T interface {
	fmt.Stringer
	util.Element[T]
}](
	ctx context.Context,
	text *Text,
	singular, plural string,
	failed util.Container[T],
) (err error) {
	if failed.Len() == 0 {
		return nil
	}

	p := func(f string, a ...interface{}) {
		_, e := fmt.Fprintf(text.Writer, f, a...)
		err = errör.Composite(err, e)
	}

	word := plural
	if failed.Len() == 1 {
		word = singular
	}

	p("%d failed %s", failed.Len(), word)

	if failed.Len() <= LISTMAX {
		p(
			": %s",
			text.au.BrightRed(
				strings.Join(
					util.Transform(ctx, func(t T) string {
						return t.String()
					}, failed.Slice()...), ", "),
			).Bold(),
		)
	}

	p("\n")

	return
}

type Text struct {
	Config

	au                  aurora.Aurora
	failedFiles         util.OrderedSet[*file.File]
	failedTools         util.OrderedSet[*spec.Tool]
	mtx                 sync.Mutex
	ok, fail, separator string
	statuses            map[bool]uint64
}

func NewText(config Config) *Text {
	text := &Text{
		Config: config,

		au:        aurora.NewAurora(config.Colour),
		statuses:  make(map[bool]uint64),
		ok:        string(OK),
		fail:      string(Fail),
		separator: string(Separator),
	}

	if text.ASCII {
		text.ok = string(ASCIIOK)
		text.fail = string(ASCIIFail)
		text.separator = string(ASCIISeparator)
	}

	text.ok = text.au.Green(text.ok).Bold().String()
	text.fail = text.au.Red(text.fail).Bold().String()
	text.separator = text.au.Faint(text.separator).String()

	return text
}

func (text *Text) AfterTask(ctx context.Context, res *task.Result) error {
	text.mtx.Lock()
	defer text.mtx.Unlock()

	text.statuses[res.Success]++

	if text.Verbosity <= 0 && res.Success {
		return nil
	}

	if text.Verbosity <= -1 {
		return nil
	}

	if !res.Success {
		text.failedTools.Insert(ctx, res.Tool)

		if res.File != nil {
			text.failedFiles.Insert(ctx, res.File)
		}
	}

	if res.Error != nil {
		fmt.Fprintf(
			text.Writer,
			"washer: %s failed with an error:\n%v\n",
			res.Tool.Name,
			res.Error,
		)

		return nil
	}

	errors, err := res.Errors(ctx)
	if err != nil {
		return err
	}

	for _, error := range errors {
		if error.Location.File == "" {
			fmt.Fprintf(
				text.Writer,
				"washer: %s failed:\n%s\n",
				res.Tool.Name,
				error.Message,
			)

			continue
		}

		fmt.Fprintf(
			text.Writer,
			"washer:%s: %s failed:\n%s\n",
			error.Location.String(),
			res.Tool.Name,
			error.Message,
		)
	}

	return nil
}

func (text *Text) BeforeTask(
	context.Context,
	*spec.Tool,
	*file.File,
) error {
	return nil
}

func (text *Text) Close() (err error) {
	text.mtx.Lock()
	defer text.mtx.Unlock()

	if text.Verbosity <= -2 {
		return
	}

	total := util.Sum(maps.Values(text.statuses)...)

	if len(text.statuses) > 1 {
		_, fmterr := fmt.Fprintf(
			text.Writer,
			"\nTotal: %d\n",
			total,
		)
		err = errör.Composite(err, fmterr)
	}

	ctx := context.Background()

	err = errör.Composite(
		err,
		failed[*file.File](ctx, text, "file", "files", &text.failedFiles),
	)
	err = errör.Composite(
		err,
		failed[*spec.Tool](ctx, text, "tool", "tools", &text.failedTools),
	)

	return
}

func (text *Text) Open(context.Context) error {
	return nil
}
