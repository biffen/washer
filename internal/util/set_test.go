package util_test

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/biffen/washer/internal/util"
)

//nolint:thelper // Nah.
func testSet[T util.Container[String]](
	tb testing.TB,
	ctor func(context.Context, ...String) T,
	sorted bool,
) {
	var (
		ctx      = context.Background()
		foo, bar = String("foo"), String("bar")
		s        = ctor(ctx)
	)

	assert.Empty(tb, s.Slice())
	assert.Zero(tb, s.Len())

	ok := s.Contains(ctx, foo)
	assert.False(tb, ok)

	ok = s.Contains(ctx, bar)
	assert.False(tb, ok)

	s = ctor(ctx, foo)
	assert.EqualValues(tb, []String{foo}, s.Slice())

	ok = s.Contains(ctx, foo)
	assert.True(tb, ok)

	ok = s.Contains(ctx, bar)
	assert.False(tb, ok)

	inserted := s.Insert(ctx, foo)
	assert.Equal(tb, 0, inserted)
	assert.EqualValues(tb, []String{foo}, s.Slice())

	ok = s.Contains(ctx, foo)
	assert.True(tb, ok)

	ok = s.Contains(ctx, bar)
	assert.False(tb, ok)

	inserted = s.Insert(ctx, bar)
	assert.Equal(tb, 1, inserted)

	if sorted {
		assert.EqualValues(tb, []String{bar, foo}, s.Slice())
	} else {
		assert.EqualValues(tb, []String{foo, bar}, s.Slice())
	}

	ok = s.Contains(ctx, foo)
	assert.True(tb, ok)

	ok = s.Contains(ctx, bar)
	assert.True(tb, ok)
}
