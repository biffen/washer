package util

import (
	"context"

	errör "gitlab.com/biffen/error"
	"golang.org/x/exp/constraints"
)

func ChanToSlice[T any](
	ctx context.Context,
	c <-chan T,
) (slice []T, err error) {
	slice = make([]T, 0, cap(c))

	for {
		select {
		case <-ctx.Done():
			err = ctx.Err()

			return

		case t, ok := <-c:
			if !ok {
				return
			}

			slice = append(slice, t)
		}
	}
}

func Concatenate[T any](ins ...[]T) (out []T) {
	for _, in := range ins {
		out = append(out, in...)
	}

	return
}

func Each[T any](ctx context.Context, ts []T, f func(*T) bool) error {
	return EachErr(ctx, ts, func(t *T) (bool, error) {
		return f(t), nil
	})
}

func EachErr[T any](
	ctx context.Context,
	ts []T,
	f func(*T) (bool, error),
) error {
EACH:
	for i := range ts {
		select {
		case <-ctx.Done():
			return ctx.Err()

		default:
			ok, err := f(&ts[i])
			if err != nil {
				return err
			}

			if !ok {
				break EACH
			}
		}
	}

	return nil
}

func Filter[T any](f func(T) bool, in ...T) (out []T) {
	out = make([]T, 0, len(in))

	for _, t := range in {
		if f(t) {
			out = append(out, t)
		}
	}

	return
}

func NonEmpty[T any](t []T) bool {
	return len(t) > 0
}

func SliceToChan[T any](
	ctx context.Context,
	slice []T,
	c chan<- T,
) (err error) {
	for _, t := range slice {
		select {
		case <-ctx.Done():
			err = ctx.Err()

			return

		case c <- t:
		}
	}

	return nil
}

func Sum[T constraints.Ordered](terms ...T) (sum T) {
	for _, term := range terms {
		sum += term
	}

	return
}

func Transform[From, To any](
	ctx context.Context,
	transformer func(From) To,
	from ...From,
) (to []To) {
	to, _ = TransformErr(ctx, func(f From) (To, error) {
		return transformer(f), nil
	}, from...)

	return
}

func TransformErr[From, To any](
	ctx context.Context,
	transformer func(From) (To, error),
	from ...From,
) ([]To, error) {
	var (
		errors error
		to     = make([]To, 0, len(from))
	)

	for _, f := range from {
		select {
		case <-ctx.Done():
			return nil, ctx.Err()

		default:
			if t, err := transformer(f); err == nil {
				to = append(to, t)
			} else {
				errors = errör.Composite(errors, err)
			}
		}
	}

	return to, errors
}
