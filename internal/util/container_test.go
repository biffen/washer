package util_test

import (
	"strings"
)

type String string

func (str String) Compare(other String) int {
	return strings.Compare(string(str), string(other))
}
