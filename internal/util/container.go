package util

import (
	"context"
)

func Any[T Element[T]](
	ctx context.Context,
	c Container[T],
	f func(T) bool,
) (ok bool, err error) {
	err = c.Each(ctx, func(t T) bool {
		ok = f(t)

		return !ok
	})

	return
}

func ElementsEqual[T Element[T]](a, b T) bool {
	return a.Compare(b) == 0 &&
		b.Compare(a) == 0
}

type Container[T Element[T]] interface {
	Contains(context.Context, T) bool
	Each(context.Context, func(T) bool) error
	Empty() bool
	Insert(context.Context, ...T) int
	Len() int
	Slice() []T
}

type Element[T any] interface {
	Compare(T) int
}
