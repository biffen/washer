package codeclimate

import (
	"crypto/sha512"
	"encoding/hex"
	"fmt"
	"strconv"
)

// https://github.com/codeclimate/platform/blob/master/spec/analyzers/SPEC.md#data-types

const (
	BugRisk       Category = "Bug Risk"
	Clarity       Category = "Clarity"
	Compatibility Category = "Compatibility"
	Complexity    Category = "Complexity"
	Duplication   Category = "Duplication"
	Performance   Category = "Performance"
	Security      Category = "Security"
	Style         Category = "Style"

	Info     Severity = "info"
	Minor    Severity = "minor"
	Major    Severity = "major"
	Critical Severity = "critical"
	Blocker  Severity = "blocker"
)

var (
	_ Position     = (*LineAndColumn)(nil)
	_ Position     = (*Offset)(nil)
	_ fmt.Stringer = (*Lines)(nil)
	_ fmt.Stringer = (*Location)(nil)
)

type Category string

type Content struct {
	Body string `json:"body"`
}

type Issue struct {
	Type              string     `json:"type"`
	CheckName         string     `json:"check_name"`
	Description       string     `json:"description"`
	Content           *Content   `json:"content,omitempty"`
	Categories        []Category `json:"categories"`
	Location          Location   `json:"location"`
	OtherLocations    []Location `json:"other_locations,omitempty"`
	Trace             *Trace     `json:"trace,omitempty"`
	RemediationPoints uint64     `json:"remediation_points,omitempty"`
	Severity          Severity   `json:"severity,omitempty"`
	Fingerprint       string     `json:"fingerprint,omitempty"`
}

func NewIssue(
	checkName, description string,
	categories []Category,
	location Location,
) (*Issue, error) {
	i := &Issue{
		Type:        "issue",
		CheckName:   checkName,
		Description: description,
		Categories:  categories,
		Location:    location,
	}

	if i.Location.Path == "" {
		i.Location.Path = "."
	}

	s := sha512.New()
	if _, err := fmt.Fprintf(s, `%s:%s:%s:%s`,
		i.Type,
		i.CheckName,
		i.Description,
		i.Location,
	); err != nil {
		return nil, err
	}

	i.Fingerprint = hex.EncodeToString(s.Sum(nil))

	return i, nil
}

type LineAndColumn struct{}

type Lines struct {
	Begin uint64 `json:"begin,omitempty"`
	End   uint64 `json:"end,omitempty"`
}

func (l Lines) String() string {
	if l.End == 0 {
		return strconv.FormatUint(l.Begin, 10)
	}

	return fmt.Sprintf("%d-%d", l.Begin, l.End)
}

type Location struct {
	Path  string `json:"path"`
	Lines Lines  `json:"lines,omitempty"`
}

func (l Location) String() string {
	return fmt.Sprintf("%s:%s", l.Path, l.Lines)
}

type Offset struct{}

type Position interface{}

type Positions struct {
	Begin Position `json:"begin,omitempty"`
	End   Position `json:"end,omitempty"`
}

type Severity string

type Trace struct {
	Locations  []Location `json:"locations,omitempty"`
	Stacktrace bool       `json:"stacktrace,omitempty"`
}
