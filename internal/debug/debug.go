package debug

import (
	"fmt"
	"os"
	"sync"
)

var (
	f   func(format string, a ...any)
	mtx sync.Mutex
)

func Printf(format string, args ...any) {
	if f == nil {
		return
	}

	mtx.Lock()
	defer mtx.Unlock()

	printf(format, args...)
}

func init() {
	if os.Getenv("WASHER_DEBUG") != "" {
		f = printf
	}
}

func printf(format string, args ...any) {
	fmt.Fprintf(os.Stderr, "washer: "+format+"\n", args...)
}
