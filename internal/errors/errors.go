package errors

import (
	"errors"
	"fmt"
)

const (
	ErrFailure = Error("failure")
	ErrUser    = Error("user error")

	ExitOK      = 0
	ExitGeneral = 1
	ExitUser    = 2
	ExitFailure = 3
	ExitCancel  = 4
)

var _ error = Error("")

func ExitCode(err error) int {
	switch {
	case err == nil:
		return ExitOK

	case errors.Is(err, ErrUser):
		return ExitUser

	case errors.Is(err, ErrFailure):
		return ExitFailure

	default:
		return ExitGeneral
	}
}

func Internal(err error) error {
	if err == nil {
		return errors.New("internal error")
	}

	return fmt.Errorf("internal error: %w", err)
}

type Error string

func (e Error) Error() string {
	return string(e)
}
