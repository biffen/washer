package location

import (
	"fmt"
)

var _ fmt.Stringer = (*Error)(nil)

type Error struct {
	Location
	Message string `regexp:"message"`
}

func (e *Error) String() string {
	if e.Location.File == "" {
		return e.Message
	}

	return fmt.Sprintf("%s: %s", e.Location.String(), e.Message)
}
