package location

import (
	"fmt"
)

var _ fmt.Stringer = (*Location)(nil)

type Location struct {
	File    string  `regexp:"file"`
	File2   string  `regexp:"file2"`
	Line    uint64  `regexp:"line"`
	Line2   uint64  `regexp:"line2"`
	Column  *uint64 `regexp:"column"`
	Column2 *uint64 `regexp:"column2"`
}

func (l *Location) String() string {
	// One file
	if l.File2 == "" {
		// One line
		if l.Line2 == 0 {
			if l.Column == nil {
				// No column
				return fmt.Sprintf("%s:%d", l.File, l.Line)
			}

			if l.Column2 == nil {
				// One column
				return fmt.Sprintf("%s:%d.%d", l.File, l.Line, *l.Column)
			}

			// Two columns
			return fmt.Sprintf(
				"%s:%d.%d-%d",
				l.File,
				l.Line,
				*l.Column,
				*l.Column2,
			)
		}

		// Two lines
		if l.Column == nil || l.Column2 == nil {
			// No columns
			return fmt.Sprintf("%s:%d-%d", l.File, l.Line, l.Line2)
		}

		// Two lines, two columns
		return fmt.Sprintf(
			"%s:%d.%d-%d.%d",
			l.File,
			l.Line,
			*l.Column,
			l.Line2,
			*l.Column2,
		)
	}

	// Two files
	if l.Line == 0 || l.Line2 == 0 {
		// No lines
		return fmt.Sprintf("%s-%s", l.File, l.File2)
	}

	// No columns
	if l.Column == nil || l.Column2 == nil {
		return fmt.Sprintf("%s:%d-%s:%d", l.File, l.Line, l.File2, l.Line2)
	}

	// Lines and columns
	return fmt.Sprintf(
		"%s:%d.%d-%s:%d.%d",
		l.File,
		l.Line,
		*l.Column,
		l.File2,
		l.Line2,
		*l.Column2,
	)
}
