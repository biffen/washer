package data

import (
	"context"
	"fmt"
	"io"
	"os"
	"path/filepath"

	errör "gitlab.com/biffen/error"
	"gitlab.com/biffen/washer/internal/backup"
	"gitlab.com/biffen/washer/internal/util"
)

const SchemeFile = "file"

var (
	_ io.WriteCloser = (*FileWriter)(nil)
	_ util.Pather    = (*FileWriter)(nil)
)

func NewFileReader(
	_ context.Context,
	path string,
	o *opts,
) (*os.File, error) {
	if o.WorkingDirectory != "" && !filepath.IsAbs(path) {
		path = filepath.Join(o.WorkingDirectory, path)
	}

	file, err := os.Open(path)
	if err != nil {
		return nil, fmt.Errorf("failed to open file %q: %w", path, err)
	}

	return file, nil
}

type FileWriter struct {
	file   *os.File
	backup *backup.Backup
}

func NewFileWriter(
	ctx context.Context,
	path string,
	o *opts,
) (*FileWriter, error) {
	var (
		err error
		fw  = new(FileWriter)
	)

	if o.Backup != "" {
		fw.backup, err = backup.New(ctx, path, o.Backup)
		if err != nil {
			return nil, fmt.Errorf("failed to back up %q: %w", path, err)
		}
	}

	fileMode := o.FileMode
	if fileMode == 0 {
		fileMode = 0o600
	}

	if o.Mkdir {
		dir := filepath.Dir(path)
		if err = os.MkdirAll(dir, fileMode); err != nil {
			return nil,
				fmt.Errorf("can’t create directory for file %q: %w", path, err)
		}
	}

	fw.file, err = os.OpenFile(path, os.O_RDWR|os.O_CREATE, fileMode)
	if err != nil {
		return nil, fmt.Errorf("can’t create writer for file %q: %w", path, err)
	}

	return fw, nil
}

func (fw *FileWriter) Close() (err error) {
	if fw.backup != nil {
		err = errör.Composite(fw.backup.Close())
	}

	err = errör.Composite(err, fw.file.Close())

	return
}

func (fw *FileWriter) Path() (string, error) {
	return filepath.Abs(fw.file.Name())
}

func (fw *FileWriter) Write(p []byte) (n int, err error) {
	return fw.file.Write(p)
}
