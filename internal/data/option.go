package data

import (
	"net/http"
	"os"
)

type Option struct {
	apply func(*opts) error
}

func OptionBackup(identifier string) Option {
	return Option{func(o *opts) error {
		o.Backup = identifier

		return nil
	}}
}

func OptionFileMode(mode os.FileMode) Option {
	return Option{func(o *opts) error {
		o.FileMode = mode

		return nil
	}}
}

func OptionHTTPClient(client *http.Client) Option {
	return Option{func(o *opts) error {
		o.HTTPClient = client

		return nil
	}}
}

func OptionMkdir(mkdir bool) Option {
	return Option{func(o *opts) error {
		o.Mkdir = mkdir

		return nil
	}}
}

func OptionStderr(stderr bool) Option {
	return Option{func(o *opts) error {
		o.Stderr = stderr

		return nil
	}}
}

func OptionWorkingDirectory(wd string) Option {
	return Option{func(o *opts) error {
		o.WorkingDirectory = wd

		return nil
	}}
}

type opts struct {
	Backup           string
	WorkingDirectory string
	HTTPClient       *http.Client
	FileMode         os.FileMode
	Mkdir            bool
	Stderr           bool
}

func getOpts(options ...Option) (*opts, error) {
	o := new(opts)

	for _, option := range options {
		if err := option.apply(o); err != nil {
			return nil, err
		}
	}

	return o, nil
}
