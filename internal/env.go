package internal

const (
	EnvWasherDebug         = "WASHER_DEBUG"
	EnvWasherNoTool        = "WASHER_NO_TOOL"
	EnvWasherProgress      = "WASHER_PROGRESS"
	EnvWasherSpecification = "WASHER_SPECIFICATION"
)
