package progress

import (
	"fmt"
	"io"

	"github.com/logrusorgru/aurora/v3"
	"gitlab.com/biffen/washer/internal"
	"gitlab.com/biffen/washer/internal/errors"
)

type Config struct {
	Aurora aurora.Aurora
	Writer io.Writer
}

type Progress interface {
	internal.Observer
}

func New(name string, config Config) (Progress, error) {
	switch name {
	case "", "none":
		return nil, nil

	case ProgressBar:
		return NewBar(config), nil

	case ProgressCount:
		return NewCount(config), nil

	default:
		return nil,
			fmt.Errorf("%w: unknown progress %q", errors.ErrUser, name)
	}
}

type State struct {
	Done, Total, Failed uint64
}
