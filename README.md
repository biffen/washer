# 🞉 Washer

> ⚠ Washer is still in early development. It might work, it might destroy your
> code! Only use it on code under version control.

Washer is a tool to run checks on a tree of files, e.g. a repository.

It can be used to run linters, formatters, static analysis, etc. on a whole
project, both manually and automatically, e.g. by Git hooks or in CI.

For more information see [the documentation].

```mermaid
graph LR
  W(Washer)

  subgraph Tools
  L1(Linter A) ; L2(Linter B) ; Fo(Formatter)
  end

  subgraph Files
  F1[File A] ; F2[File B] ; F3[File C]
  end

  L1 ---- W  ; L2 ---- W  ; Fo ---- W
  W  ---- F1 ; W  ---- F2 ; W  ---- F3
```

## 💾 Installation

```sh
go install gitlab.com/biffen/washer@latest
```

## ⌨ Usage

See [Usage](docs/usage.txt).

## 🏃 Quick Start

1.  Install Washer (see above)

2.  Create a file called `washer.yaml` in the root directory of your project
    with the following content:

    ```yaml
    %YAML 1.1
    ---
    exclude:
        - '.*/'

    tools:
        yamllint:
            check:
                - yamllint
                - --format=parsable
            include:
                - '*.y{a,}ml'
                - content: '^%YAML\s+\d+(?:\.\d+)?'
    ```

3.  Run Washer (the example above also requires [YAMLLint]):

    ```sh
    washer
    ```

4.  Add more tools!

## 🪲 Bugs

Please report bugs [here][issues].

<!-- ## 🧑‍💻 Contributing -->

## 📜 License

<!-- textlint-disable -->

Copyright © 2020, 2021, 2022 Theo Willows

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <https://www.gnu.org/licenses/>.

<!-- textlint-enable -->

[issues]: https://gitlab.com/biffen/washer/-/issues
[logo]: docs/img/logo.svg
[the documentation]: https://biffen.gitlab.io/washer
[yamllint]: http://www.yamllint.com
