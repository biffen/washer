.POSIX:
.SUFFIXES:

SHELL                           = /bin/sh

prefix                          = /usr/local
exec_prefix                     = $(prefix)
bindir                          = $(exec_prefix)/bin
sbindir                         = $(exec_prefix)/sbin
libexecdir                      = $(exec_prefix)/libexec

CACHE                           = .cache
DOCS                            = docs
GIT                             = git
GO                              = go
GO_TEST                         = $(GO) test -covermode atomic -coverpkg "$$(go list)/..." -coverprofile coverage.txt $(GO_TEST_FLAGS) ./...
GO_TEST_FLAGS                   = -race -v
SPHINX_BUILD                    = sphinx-build
SPHINX_BUILD_FLAGS              = --color --keep-going -D release="$${CI_COMMIT_TAG:-DEV}" -D version="$${CI_COMMIT_TAG:-DEV}" -W -d $(CACHE)/doctrees -j auto -n $${CI_DEBUG_TRACE:+-vvv}

DOC_FILES                      != $(GIT) ls-files $(DOCS)/
GO_FILES                       != $(GIT) ls-files '*.go'

all: build

build: washer

check: test lint

clean: mostlyclean
	rm -fr $(CACHE)

coverage: coverage.html

lint:
	$(GO) run .

mostlyclean:
	rm -fr coverage.* html washer

test:
	$(GO_TEST)

.PHONY: all build check clean coverage lint mostlyclean test

coverage.html: coverage.txt
	$(GO) tool cover -func $<
	$(GO) tool cover -html $< -o coverage.html

coverage.txt: test

coverage.xml: coverage.txt
	gocover-cobertura < $< > $@

html: $(DOC_FILES)
	$(info $(DOC_FILES))
	$(SPHINX_BUILD) $(SPHINX_BUILD_FLAGS) -b html $(DOCS) $@

junit.xml: go.mod go.sum $(GO_FILES)
	$(GO_TEST) | go-junit-report -set-exit-code > $@

washer: go.mod go.sum $(GO_FILES)
	$(GO) build -o washer .
